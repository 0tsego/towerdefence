﻿using UnityEngine;
using System.Collections.Generic;

public class GlDrawer : MonoBehaviour
{
    public static GlDrawer Instance { get; private set; }
    private static Material _lineMaterial;
    private List<CircleData> _circlesToDraw = new List<CircleData>();

    private void Awake()
    { 
        if(Instance != null)
        {
            Debug.Log("You're trying to create additional instance of GlDrawer. This is not allowed!");
            Destroy(gameObject);
            return;
        }

        Instance = this;
    }

    static void CreateLineMaterial()
    {
        if (!_lineMaterial)
        {
            // Unity has a built-in shader that is useful for drawing
            // simple colored things.
            Shader shader = Shader.Find("Hidden/Internal-Colored");
            _lineMaterial = new Material(shader);
            _lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            // Turn on alpha blending
            _lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            _lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            // Turn backface culling off
            _lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            // Turn off depth writes
            _lineMaterial.SetInt("_ZWrite", 0);
        }
    }

    // Will be called after all regular rendering is done
    public void OnRenderObject()
    {
        CreateLineMaterial();
        // Apply the line material
        _lineMaterial.SetPass(0);

        GL.PushMatrix();
        // Set transformation matrix for drawing to
        // match our transform
        GL.MultMatrix(transform.localToWorldMatrix);

        //stuff to draw
        DrawCircles();

        GL.PopMatrix();
    }

    public void ShowCircle(Vector3 center, float radius)
    {
        var circle = new CircleData
        {
            center = center,
            radius = radius,
            color = Color.cyan
        };

        _circlesToDraw.Add(circle);
    }

    public void ShowCircle(Vector3 center, float radius, Color color)
    {
        var circle = new CircleData
        {
            center = center,
            radius = radius,
            color = color
        };

        _circlesToDraw.Add(circle);
    }

    private void DrawCircles()
    {
        foreach (var circle in _circlesToDraw)
            DrawCircle(circle);
    }

    private void DrawCircle(CircleData circle)
    {
        GL.Begin(GL.LINES);
        GL.Color(circle.color);
        for (var theta = 0.0f; theta < (2f * Mathf.PI); theta += 0.01f)
        {
            var ci = (new Vector3(Mathf.Cos(theta) * circle.radius + circle.center.x, Mathf.Sin(theta) * circle.radius + circle.center.y, circle.center.z));
            GL.Vertex3(ci.x, ci.y, ci.z);
        }
        GL.End();
    }
}