﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiledLineRenderer : MonoBehaviour
{
    private Renderer _renderer;
    private Material _material;
    private float _length;
    private float _tilesCount;
    private Vector3 _position;
    private float _angle;
    public Texture _texture;
    private Vector3 up = new Vector3(0f, 1f, 0f);
    private LineRenderer _lineRenderer;

    //for tests:
    public Transform[] points;
    private Vector3[] Points
    {
        get
        {
            var p = new Vector3[points.Length];

            for (var i = 0; i < points.Length; i++)
                p[i] = points[i].position;

            return p;
        }
    }

    private void Start()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        _renderer = GetComponent<Renderer>();
        _material = _renderer.material;
        _material.mainTexture = _texture;
        _lineRenderer.startWidth = _texture.height;
        _lineRenderer.endWidth = _texture.height;

        Show(Points);
    }

    public void Show(Vector3 origin, Vector3 target)
    {
        var points = new Vector3[2];
        points[0] = origin;
        points[1] = target;

        Show(points);
    }

    public void Show(Vector3[] points)
    {
        _lineRenderer.positionCount = points.Length;
        _lineRenderer.SetPositions(points);

        var lineLength = 0f;
        for (var i = 1; i < points.Length; i++)
        {
            lineLength += (points[i] - points[i - 1]).magnitude;
        }

        _material.mainTextureScale = new Vector2(lineLength / (float)_texture.width, 1f);
    }
}