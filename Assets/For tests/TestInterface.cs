﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class TestInterface : MonoBehaviour
{
    public static event Action OnStartClick;

    private void OnGUI()
    {
        if(GUI.Button(new Rect(10,10,160,80), "START"))
        {
            if (OnStartClick != null)
                OnStartClick();
        }
    }

    private void UnsubscribeAllEvents()
    {
        OnStartClick = null;
    }

    private void OnDestroy()
    {
        UnsubscribeAllEvents();
    }
}
