﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleData
{
    public Vector3 center;
    public float radius;
    public Color color;
}
