﻿using UnityEngine;

public class Ballistics : MonoBehaviour
{
    private float h = 200f; //высота полета в максимальной точке
    private float g = 9.81f; //ускорение свободного падения

    private float alpha, t, V0, x, y, z, curS; //для расчета баллистики
    private Vector3 _direction;
    private Vector3 startPos, curPos; //стартовая позиция объекта; текущая позиция (преобразованная изометрически)
    private bool bInit; //если true, то происходит движение
    private float _flyTime;
    private float accel;
    private float elapsedTime;


    void Update()
    {
        Fly();
    }

    public void Throw(Vector3 targetPosition, float flyTime) //функция инициализации. Используется, помимо прочего, извне - для первого запуска
    {
        _flyTime = flyTime;
        t = 0;
        curS = 0;
        curPos = new Vector3();
        startPos = transform.position;

        //рассчеты, необходимые для поиска начальных угла и скорости
        //выкладки произведены вручную, здесь уже записаны результирующие формулы
        _direction = (targetPosition - startPos).normalized;
        var distance = (targetPosition - startPos).magnitude;
        var D = 64f * h * h - 4f * (distance + 1f) * (1f - distance);
        alpha = Mathf.Atan((8f * h + Mathf.Sqrt(D)) / (2f * (distance + 1f)));
        V0 = Mathf.Sqrt(distance * g / Mathf.Sin(2f * alpha));
        var estimatedFlyTime = 2f * V0 * Mathf.Sin(alpha) / g;
        accel = estimatedFlyTime / flyTime;

        bInit = true;
    }

    protected void Fly()
    {
        if (bInit)
        {
            if (elapsedTime > _flyTime) //летим пока не упадем
            {
                Destroy(gameObject);
                return;
            }

            //баллистическое движение рассчитывается по соответствующим формулам
            elapsedTime += Time.deltaTime;
            t += accel * Time.deltaTime; //текущее время полета

            curS = V0 * t * Mathf.Cos(alpha); //пройденное расстояние по прямой, другими словами проекция на землю
            x = curS * _direction.x; //умножаем пройденное расстояние на коэффициенты направления
            y = curS * _direction.y;
            z = V0 * t * Mathf.Sin(alpha) - (g * t * t) / 2f; //рассчитываем положение в воздухе

            //коэффициенты преобразования координат в изометрическую проекцию
            //поворот производится относительно оси Х
            //координата 'x' не претерпевает изменений, 
            //а 'y' считается по формуле curPos.y = cosa * y + sina * z,
            //где 'a' - угол поворота относительно оси x
            curPos.x = x;

            //curPos.y = y * Mathf.Cos(Mathf.Deg2Rad * a) + z * Mathf.Sin(Mathf.Deg2Rad * a); //раскомментировать, чтобы использовать угол, отличный от рассчитаных вручную ниже
            //curPos.y = 0.9063f * y + 0.4226f * z; // a = 25 градусов
            curPos.y = 0.891f * y + 0.454f * z; // a = 27 градусов
                                                //curPos.y = 0.5f * (1.732f * y + z); // a = 30 градусов
                                                //curPos.y = 0.8191f * y + 0.5735f * z; // a = 35 градусов
                                                //curPos.y = 0.7071f * (y + z); // a = 45 градусов

            transform.position = startPos + curPos; //задание позиции с учетом изометрии
        }
    }
}