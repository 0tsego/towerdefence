﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiledSpriteRenderer : MonoBehaviour
{
    private Renderer _renderer;
    private Material _material;
    private float _length;
    private float _tilesCount;
    private Vector3 _position;
    private float _angle;
    public Texture _texture;
    private Vector3 up = new Vector3(0f, 1f, 0f);

    //for tests:
    public Transform t1;
    public Transform t2;

    private void Start()
    {
        _renderer = GetComponent<Renderer>();
        _material = _renderer.material;
        _material.mainTexture = _texture;

        Show(t1.position, t2.position);
    }

    public void Show(Vector3 origin, Vector3 target)
    {
        var diff = target - origin;
        _length = Mathf.Abs(diff.x) > Mathf.Abs(diff.y) ? Mathf.Abs(diff.x) : Mathf.Abs(diff.y);
        _position = origin + diff / 2f;
        transform.position = _position;
        transform.localScale = new Vector3(_texture.width, _length, 1f);
        _tilesCount = _length / _texture.height;
        _material.mainTextureScale = new Vector2(1f, _tilesCount);
        _angle = Vector3.Angle(diff, up);
        transform.rotation = Quaternion.Euler(0f, 0f, _angle);
    }
}