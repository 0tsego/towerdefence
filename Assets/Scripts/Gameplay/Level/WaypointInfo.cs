﻿using UnityEngine;

public class WaypointInfo
{
    public Vector2 position;
    public float distanceFromStart;
    public float pathProgress;

    public override string ToString()
    {
        var info = string.Format("Waypoint info: position = {0}; distanceFromStart = {1}; pathProgress = {2}", position, distanceFromStart, pathProgress);
        return info;
    }
}