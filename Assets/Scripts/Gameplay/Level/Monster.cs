﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class Monster : MonoBehaviour 
{
    private const float MOVEMENT_SPEED = 30f;

    public event Action<Monster> OnDeath;

    private PathInfo _pathInfo;
    private SpriteAnimator _animator;
    private int _currentWaypointIndex = 1;
    private bool _movementEnabled;
    private Vector2 _prevDir;
    private float _coveredDistance;
    private Vector2 _currentDirection;
    private AnimationDirection _lastAnimDirection = AnimationDirection.Unknown;
    private MonsterData _monsterData;

    public float PathProgress
    {
        get
        {
            return _coveredDistance / _pathInfo.Length;
        }
    }

    public void StartMovement(Vector2[] waypointPositions)
    {
        if(waypointPositions == null || waypointPositions.Length < 2)
        {
            Debug.LogError("There is no way to go! Transform instance id is " + transform.GetInstanceID());
            return;
        }

        _pathInfo = new PathInfo(waypointPositions);
        _monsterData = GameDataLibrary.Instance.GetMonsterData(name);
        transform.position = waypointPositions[0];
        _prevDir = _currentDirection = CalculateCurrentDirection();
        SetupAnimator();
        PlayAppropriateWalkAnimation();
        _movementEnabled = true;
    }

    public Vector2 GetEstimatedPosition(float deltaTime)
    {
        var estimatedDistance = _coveredDistance + MOVEMENT_SPEED * deltaTime;
        return _pathInfo.CalculateEstimatedPosition(estimatedDistance);
    }

    private void SetupAnimator()
    {
        _animator = GetComponent<SpriteAnimator>();

        _animator.AddAnimation(AnimationDirection.North, _monsterData.GetWalkNorthAnim());
        _animator.AddAnimation(AnimationDirection.South, _monsterData.GetWalkSouthAnim());
        _animator.AddAnimation(AnimationDirection.Side, _monsterData.GetWalkSideAnim());
    }

    private void Update()
    {
        if (!_movementEnabled)
            return;

        if (PathProgress > _pathInfo.GetProgress(_currentWaypointIndex))
        {
            if (_pathInfo.GetProgress(++_currentWaypointIndex) > 1f)
            {
                _movementEnabled = false;
                Kill();
                return;
            }

            _prevDir = _currentDirection;
            _currentDirection = CalculateCurrentDirection();
            PlayAppropriateWalkAnimation();
        }

        var deltaDist = MOVEMENT_SPEED * Time.deltaTime;
        _coveredDistance += deltaDist;
        var deltaPos = _currentDirection.normalized * deltaDist;

        transform.position += (Vector3)deltaPos;
    }

    private Vector2 CalculateCurrentDirection()
    {
        return _pathInfo.GetWaypointPosition(_currentWaypointIndex) - (Vector2)transform.position;
    }

    private bool IsDirectionStillTheSame
    {
        get
        {
            var dir = CalculateCurrentDirection();
            return Mathf.Sign(dir.x) == Mathf.Sign(_prevDir.x) && Mathf.Sign(dir.y) == Mathf.Sign(_prevDir.y);
        }
    }

    private void Kill()
    {
        Destroy(gameObject);
        if (OnDeath != null)
        {
            OnDeath(this);
            OnDeath = null;
        }
    }

    public void SetDamage()
    {
        Kill();
    }

    private void PlayAppropriateWalkAnimation()
    {
        var animDir = CalculateAnimationDirection(_currentDirection);
        if (animDir != _lastAnimDirection)
        {
            _lastAnimDirection = animDir;
            _animator.Play(animDir);
        }
        SetCurrentSideOrientation();
    }

    private void SetCurrentSideOrientation()
    {
        var scale = transform.localScale;
        if (_lastAnimDirection == AnimationDirection.Side)
            scale.x = Mathf.Sign(_currentDirection.x);
        else
            scale.x = 1f;
        transform.localScale = scale;
    }

    private AnimationDirection CalculateAnimationDirection(Vector2 direction)
    {
        var angle = Vector2.Angle(Vector2.right, direction);

        //Debug.Log("angle = " + angle);
        if (angle < 70f || (angle > 110f && angle <= 180f))
            return AnimationDirection.Side;

        if (direction.y > 0f)
            return AnimationDirection.North;
        else
            return AnimationDirection.South;
    }
}