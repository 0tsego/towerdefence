﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallisticProjectile : Projectile
{
    private Ballistics _ballistics;
    public override void Initialize(ProjectileData data, Vector3 position, Monster target)
    {
        base.Initialize(data, position, target);

        _ballistics = GetComponent<Ballistics>();
        _ballistics.Throw(target.GetEstimatedPosition(2f), 2f);
    }

    protected override void Translate()
    {
    }

    protected override bool IsTargetReached
    {
        get
        {
            return false;
        }
    }
}