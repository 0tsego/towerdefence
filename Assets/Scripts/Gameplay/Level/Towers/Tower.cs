﻿using UnityEngine;
using System.Collections;
using System;

public class Tower : MonoBehaviour, ITower 
{
    public TowerState State { get; set; }

    [SerializeField]
    private TowerData _data;
    private SpriteAnimator _idleAnimator;
    private SpriteAnimator _attackAnimator;
    private MapController _mapController;
    private Monster _target;

    public void Initialize(MapController mapController, string towerName, int level)
    {
        _mapController = mapController;
        name = string.Format("{0}_{1}", towerName, level.ToString());
        _data = GameDataLibrary.Instance.GetTowerData(towerName);
        _data.currentLevel = level;
        SetupAnimator();
        State = new IdleState(this);
        GlDrawer.Instance.ShowCircle(transform.position, _data.Range);
    }

    private void SetupAnimator()
    {
        _idleAnimator = gameObject.AddComponent<SpriteAnimator>();
        _idleAnimator.AddAnimation(_data.GetIdleAnim());

        _attackAnimator = gameObject.AddComponent<SpriteAnimator>();
        _attackAnimator.AddAnimation(_data.GetAttackAnim());
    }

    //ATTACK
    public void StartAttack(Action onFinish)
    {
        if (_target != null)
        {
            var projectileData = GameDataLibrary.Instance.GetProjectilesData(_data.ProjectileName);
            var projectilePrefab = PrefabLoader.LoadGameObjectFromResources(projectileData.type);
            var projectileGameobject = Instantiate(projectilePrefab);
            var projectile = projectileGameobject.GetComponent<Projectile>();
            projectile.Initialize(projectileData, transform.position + (Vector3)_data.AttackPoint, _target);
            _idleAnimator.Stop();
            _attackAnimator.Play();
        }

        if (onFinish != null)
            onFinish();
    }

    //RECHARGING
    public void StartRecharging(Action onFinish)
    {
        StartCoroutine(RechargingCrtn(onFinish));
    }

    private IEnumerator RechargingCrtn(Action onFinish)
    {
        yield return new WaitForSeconds(1f);

        if (onFinish != null)
            onFinish();
    }

    //ENEMY SEARCHING
    public void StartEnemySearching(Action onFinish)
    {
        _attackAnimator.Stop();
        _idleAnimator.Play();
        StartCoroutine(EnemySearchCrtn(onFinish));
    }

    private IEnumerator EnemySearchCrtn(Action onFinish)
    {
        while (true)
        {
            _mapController.waveController.TryGetMonsterWhichClosestToFinish(transform.position, _data.SqrRange, out _target);

            if (_target != null)
            {
                if (onFinish != null)
                    onFinish();

                yield break;
            }

            yield return new WaitForSeconds(0.1f);
        }
    }
}