﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState: TowerState
{
    public IdleState(ITower tower): base(tower){}

    protected override void Enter()
    {
        _tower.StartEnemySearching(Exit);
    }

    protected override void Exit()
    {
        _tower.State = new AttackState(_tower);
    }
}