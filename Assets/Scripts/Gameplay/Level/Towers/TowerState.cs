﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class TowerState
{
    protected ITower _tower;

    public TowerState(ITower tower)
    {
        _tower = tower;
        Enter();
    }

    protected abstract void Enter();
    protected abstract void Exit();
}