﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public interface ITower
{
    TowerState State { get; set; }
    void StartAttack(Action onFinish);
    void StartRecharging(Action onFinish);
    void StartEnemySearching(Action onFinish);
}