﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RechargeState : TowerState
{
    public RechargeState(ITower tower): base(tower){}

    protected override void Enter()
    {
        _tower.StartRecharging(Exit);
    }

    protected override void Exit()
    {
        _tower.State = new IdleState(_tower);
    }
}