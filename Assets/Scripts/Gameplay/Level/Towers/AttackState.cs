﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : TowerState
{
    public AttackState(ITower tower): base(tower){}

    protected override void Enter()
    {
        _tower.StartAttack(Exit);
    }

    protected override void Exit()
    {
        _tower.State = new RechargeState(_tower);
    }
}