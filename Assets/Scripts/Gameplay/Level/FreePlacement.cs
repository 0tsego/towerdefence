﻿using UnityEngine;

public class FreePlacement : MonoBehaviour 
{
    private Tower _currentTower;

    public bool IsFree
    {
        get
        {
            return _currentTower == null;
        }
    }

    public void AddTower(Tower tower)
    {
        if (!IsFree)
            return; 

        _currentTower = tower;
    }
}