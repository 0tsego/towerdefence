﻿using UnityEngine;

public class Projectile : MonoBehaviour 
{
    private const float MIN_TARGET_DISTANCE = 50f;

    private Vector3 _direction;
    protected Monster _target;
    protected ProjectileData _data;
    private SpriteAnimator _spriteAnimator;

    public virtual void Initialize(ProjectileData data, Vector3 position, Monster target)
    {
        _data = data;
        transform.position = position;
        _target = target;

        _spriteAnimator = gameObject.AddComponent<SpriteAnimator>();
        var frames = _data.GetMainAnimation();
        _spriteAnimator.AddAnimation(frames);
        _spriteAnimator.Play();
    }

    private void Update()
    {
        if(_target == null)
        {
            Destroy(gameObject);
            return;
        }

        if (IsTargetReached)
        {
            SetDamageToTarget();
            Destroy(gameObject);
            return;
        }

        Translate();
    }

    protected virtual void Translate()
    {
        _direction = (_target.transform.position - transform.position).normalized;
        var translation = _direction * _data.speed * Time.deltaTime;
        transform.Translate(translation);
    }

    virtual protected bool IsTargetReached
    {
        get
        {
            var sqrDist = (_target.transform.position - transform.position).sqrMagnitude;
            return MIN_TARGET_DISTANCE > sqrDist;
        }
    }

    private void SetDamageToTarget()
    {
        if (_target == null)
            return;

        _target.SetDamage();
    }
}