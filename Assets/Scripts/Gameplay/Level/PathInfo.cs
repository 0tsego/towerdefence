﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathInfo
{
    public readonly float Length;

    private WaypointInfo[] _waypoints;

    public PathInfo(Vector2[] waypointPositions)
    {
        _waypoints = new WaypointInfo[waypointPositions.Length];
        Length = CalculatePathLength(waypointPositions);
        FillWaypoints(waypointPositions);
    }

    public int CalculateLastPassedWaypointIndex(float passedDistance)
    {
        for(var i = _waypoints.Length - 1; i >= 0; i--)
        {
            if (passedDistance >= _waypoints[i].distanceFromStart)
                return i;
        }

        return -1;
    }

    public Vector2 CalculateEstimatedPosition(float passedDistance)
    {
        var passedWaypointIndex = CalculateLastPassedWaypointIndex(passedDistance);

        if(passedWaypointIndex < 0)
        {
            Debug.LogWarning("Can't find a waypoint");
            return Vector2.zero;
        }

        if(passedWaypointIndex >= _waypoints.Length - 1)
        {
            Debug.LogWarning("This is the last waypoint");
            return Vector2.zero;
        }

        var passedWaypoint = _waypoints[passedWaypointIndex];
        var nextWaypoint = _waypoints[passedWaypointIndex + 1];

        var diffBetweenWaypoints = nextWaypoint.position - passedWaypoint.position;
        var localDistance = passedDistance - passedWaypoint.distanceFromStart;

        var estimatedPosition = passedWaypoint.position + localDistance * diffBetweenWaypoints.normalized;

        return estimatedPosition;
    }

    public float GetProgress(int index)
    {
        if (index >= _waypoints.Length)
            return 2f;

        return _waypoints[index].pathProgress;
    }

    public Vector2 GetWaypointPosition(int index)
    {
        return _waypoints[index].position;
    }

    private float CalculatePathLength(Vector2[] waypointPositions)
    {
        if (waypointPositions.Length < 2)
            return 0f;

        var length = 0f;
        var pos = waypointPositions[0];
        for (var i = 1; i < waypointPositions.Length; i++)
        {
            length += (waypointPositions[i] - pos).magnitude;
            pos = waypointPositions[i];
        }

        return length;
    }

    private void FillWaypoints(Vector2[] waypointPositions)
    {
        _waypoints = new WaypointInfo[waypointPositions.Length];

        var previousWaypointPosition = waypointPositions[0];
        for (var i = 0; i < _waypoints.Length; i++)
        {
            _waypoints[i] = new WaypointInfo();
            _waypoints[i].position = waypointPositions[i];

            //path progress:
            if(i > 0)
            {
                var distanceBetweenClosestWaypoints = (waypointPositions[i] - previousWaypointPosition).magnitude;
                _waypoints[i].distanceFromStart = distanceBetweenClosestWaypoints + _waypoints[i - 1].distanceFromStart;
                _waypoints[i].pathProgress = _waypoints[i].distanceFromStart / Length;
                previousWaypointPosition = waypointPositions[i];
            }

            //Debug.Log(_waypoints[i].ToString());
        }
    }
}
