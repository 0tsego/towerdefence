﻿using UnityEngine;
using System;

[Serializable]
public class WaveData
{
    public float StartDelay = 1f;
    public MonsterGroupData[] MonsterGroups;
}