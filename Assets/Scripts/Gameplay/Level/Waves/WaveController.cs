﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WaveController
{
    private Monster _monsterPrefab;
    private LevelWavesData _wavesData;
    private List<Monster> _activeMonsters = new List<Monster>();
    private Dictionary<string, ObjectData[]> _paths;
    private GameObject monsterRootGameObject;

    public WaveController(LevelWavesData wavesData, Dictionary<string, ObjectData[]> paths)
    {
        _wavesData = wavesData;
        _paths = paths;
        CreateMonsterRootGameObject();

        _monsterPrefab = PrefabLoader.GetPrefab<Monster>(CONSTANTS.MONSTERS_TAG);
        SubscribeEvents();
    }

    private void CreateMonsterRootGameObject()
    {
        monsterRootGameObject = new GameObject();
        monsterRootGameObject.name = "Monsters";
        monsterRootGameObject.transform.position = Vector3.zero;
    }

    private void SubscribeEvents()
    {
        TestInterface.OnStartClick += Start;
    }

    public void Start()
    {
        Main.Instance.StartCoroutine(WaveProducerCrtn());
    }

    private Monster CreateMonster(string name)
    {
        var monster = UnityEngine.Object.Instantiate(_monsterPrefab);
        monster.name = name;
        monster.transform.SetParent(monsterRootGameObject.transform, false);
        _activeMonsters.Add(monster);
        monster.OnDeath += RemoveMonsterFromActiveList;

        return monster;
    }

    private void RemoveMonsterFromActiveList(Monster monster)
    {
        _activeMonsters.Remove(monster);
    }

    public bool TryGetMonsterWhichClosestToFinish(Vector3 towerPosition, float sqrRange, out Monster leaderMonster)
    {
        var monstersInTowerArea = FindAllMonstersInRange(towerPosition, sqrRange);
        if (monstersInTowerArea.Count == 0)
        {
            leaderMonster = null;
            return false;
        }

        leaderMonster = monstersInTowerArea[0];
        var maxDistance = float.MinValue;
        for (var i = 0; i < monstersInTowerArea.Count; i++)
        {
            if (monstersInTowerArea[i].PathProgress > maxDistance)
            {
                maxDistance = monstersInTowerArea[i].PathProgress;
                leaderMonster = monstersInTowerArea[i];
            }
        }

        return true;
    }

    public List<Monster> FindAllMonstersInRange(Vector3 origin, float sqrRange)
    {
        var monstersInRange = new List<Monster>();

        for (var i = 0; i < _activeMonsters.Count; i++)
        {
            var diff = origin - _activeMonsters[i].transform.position;
            if (diff.sqrMagnitude < sqrRange)
                monstersInRange.Add(_activeMonsters[i]);
        }

        return monstersInRange;
    }

    private IEnumerator WaveProducerCrtn()
    {
        foreach(var wave in _wavesData.Waves)
        {
            yield return new WaitForSeconds(wave.StartDelay);

            foreach(var group in wave.MonsterGroups)
            {
                yield return new WaitForSeconds(group.StartDelay);

                for (var i = 0; i < group.Amount; i++)
                {
                    var monster = CreateMonster(group.MonsterName);
                    var path = ChoosePath(group.PathName);
                    monster.StartMovement(path);

                    yield return new WaitForSeconds(group.PerMonsterDelay);
                }
            }
        }
    }

    private Vector2[] ChoosePath(string pathName)
    {
        var appropriatePaths = _paths[pathName];
        var randomIndex = UnityEngine.Random.Range(0, appropriatePaths.Length);
        var chosenPath = appropriatePaths[randomIndex].Waypoints;

        return chosenPath;
    }
}