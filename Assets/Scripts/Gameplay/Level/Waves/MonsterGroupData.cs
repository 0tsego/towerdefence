﻿using UnityEngine;
using System;

[Serializable]
public class MonsterGroupData
{
    public string MonsterName;
    public string PathName;
    public int Amount = 1;
    public float StartDelay = 0f;
    public float PerMonsterDelay = 0.1f;
}