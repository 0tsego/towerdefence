﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerVisualizer
{
    private MapData _mapData;
    private TileCreator _tileCreator = new TileCreator();
    private FreePlacement _freePlacementPrefab;

    public LayerVisualizer(MapData mapData)
    {
        _mapData = mapData;
        _freePlacementPrefab = PrefabLoader.GetPrefab<FreePlacement>(CONSTANTS.FREEPLACEMENT_TAG);
    }

    public GameObject DrawLayer(LayerData layerData)
    {
        var layerGameObject = DrawAppropriateLayer(layerData);
        if(layerGameObject != null)
            layerGameObject.name = string.Format("Layer: {0}", layerData.name);

        return layerGameObject;
    }

    private GameObject DrawAppropriateLayer(LayerData layerData)
    {
        var layerName = layerData.name.ToLower();

        if (Debug.isDebugBuild && layerName.Contains(CONSTANTS.PATHS_TAG.ToLower()))
        {
            return DrawPaths(layerData);
        }
        else if (layerName.Contains(CONSTANTS.FREEPLACEMENTS_TAG.ToLower()))
        {
            return CreateFreePlacements(layerData);
        }
        else if(layerName.Contains(CONSTANTS.LEVEL_BACKGROUND_IMAGE_TAG.ToLower()))
        {
            return DrawBackgroundImage(layerData);
        }
        else
        {
            return DrawDecor(layerData);
        }
    }

    private GameObject DrawDecor(LayerData layerData)
    {
        if (layerData.Structure == null)
            return null;

        var layerGameObject = new GameObject();

        for (var i = 0; i < layerData.Structure.GetLength(0); i++)
        {
            for (var j = 0; j < layerData.Structure.GetLength(1); j++)
            {
                var tileGO = DrawTile(layerData, i, j);
                if (tileGO != null)
                    tileGO.transform.SetParent(layerGameObject.transform, false);
            }
        }

        return layerGameObject;
    }

    private GameObject DrawTile(LayerData layerData, int i, int j)
    {
        var gid = layerData.Structure[i, j];
        if (gid == 0)
            return null;

        var tilesetData = TiledUtilities.FindTilesetData(_mapData, gid);
        var tileGameObject = _tileCreator.CreateTile(tilesetData, gid, layerData.name);

        var x = j * _mapData.tilewidth;
        var y = -i * _mapData.tileheight;
        var tilePosition = new Vector3(x, y, 0f);
        tileGameObject.transform.position = tilePosition;
        tileGameObject.name = string.Format("Tile: gid = {0}, pos = ({1},{2})", gid, i, j);

        return tileGameObject;
    }

    private GameObject CreateFreePlacements(LayerData layerData)
    {
        if (layerData.objects == null)
            return null;

        var layerGameObject = new GameObject();
        
        for (var i = 0; i < layerData.objects.Length; i++)
        {
            var placementPosition = new Vector2(layerData.objects[i].x, -layerData.objects[i].y);
            var placement = UnityEngine.Object.Instantiate(_freePlacementPrefab);
            placement.transform.position = placementPosition;
            placement.transform.SetParent(layerGameObject.transform, false);
            placement.name = "FreePlacement with id = " + layerData.objects[i].id;
        }

        return layerGameObject;
    }

    private GameObject DrawObjects(LayerData layerData)
    {
        if (layerData.objects == null)
            return null;

        var layerGameObject = new GameObject();

        for (var i = 0; i < layerData.objects.Length; i++)
        {
            var tilesetData = TiledUtilities.FindTilesetData(_mapData, layerData.objects[i].gid);
            var @object = _tileCreator.CreateTile(tilesetData, layerData.objects[i].gid, layerData.name);
            var objectName = string.Format("{0} element with name: {1}, id: {2}", layerData.name, layerData.objects[i].name, layerData.objects[i].id);
            @object.name = objectName;
            var objectPosition = new Vector2(layerData.objects[i].x, -layerData.objects[i].y);
            @object.transform.position = objectPosition;
            @object.transform.SetParent(layerGameObject.transform, false);
        }

        return layerGameObject;
    }

    private GameObject DrawPaths(LayerData layerData)
    {
        if (layerData.objects == null)
            return null;

        var layerGameObject = new GameObject();
        var waypointPrefab = Resources.Load<GameObject>("Waypoint");

        for (var i = 0; i < layerData.objects.Length; i++)
        {
            foreach(var waypointPosition in layerData.objects[i].Waypoints)
            {
                var @object = Object.Instantiate(waypointPrefab);
                var objectName = string.Format("{0} element with name: {1}, id: {2}", layerData.name, layerData.objects[i].name, layerData.objects[i].id);
                @object.name = objectName;

                @object.transform.position = waypointPosition;
                @object.transform.SetParent(layerGameObject.transform, false);
            }
        }

        return layerGameObject;
    }

    private GameObject DrawBackgroundImage(LayerData layerData)
    {
        var imageName = layerData.ImageName;
        if (string.IsNullOrEmpty(imageName))
            return null;

        var layerGameObject = new GameObject();
        var spriteRenderer = layerGameObject.AddComponent<SpriteRenderer>();
        spriteRenderer.sortingLayerName = layerData.name;
        var sprite = Resources.Load<Sprite>(imageName);
        spriteRenderer.sprite = sprite;
        var objectPosition = new Vector3(sprite.rect.width / 2f, -sprite.rect.height / 2f, 0f);
        layerGameObject.transform.position = objectPosition;
        return layerGameObject;
    }
}