﻿using UnityEngine;

public class MapVisualizer 
{
    private string _mapName;
    private MapData _mapData;
    private MapController _mapController;
    private LayerVisualizer _layerVisualizer;

    public MapVisualizer(string mapName)
    {
        _mapName = mapName;
        var mapFile = Resources.Load<TextAsset>(_mapName);
        _mapData = JsonUtility.FromJson<MapData>(mapFile.text);
        _layerVisualizer = new LayerVisualizer(_mapData);
        _mapController = new MapController(_mapName, _mapData);

        DrawMap();
        CalculateMapCenterPosition();
    }

    private void DrawMap()
    {
        var mainMapRoot = CreateRootGameObject();

        for(var index = 0; index < _mapData.layers.Length; index++)
        {
            var layerData = _mapData.layers[index];
            var layerGameObject = _layerVisualizer.DrawLayer(layerData);
            if (layerGameObject == null)
                continue;
            layerGameObject.transform.SetParent(mainMapRoot.transform, false);
        }
    }

    private GameObject CreateRootGameObject()
    {
        var root = new GameObject();
        root.name = string.Format("Map: {0}", _mapName);

        return root;
    }

    public Vector2 CalculateMapCenterPosition()
    {
        if (_mapData == null)
            return Vector2.zero;

        var x = _mapData.WidthInPixels / 2;
        var y = -_mapData.HeightInPixels / 2;

        return new Vector2(x, y);
    }

    public float CalculateRecommendOrthoSize()
    {
        var w = _mapData.WidthInPixels > 1920f ? 1920f : _mapData.WidthInPixels;
        var size = w / 2f / Screen.width * Screen.height;
        return size;
    }
}