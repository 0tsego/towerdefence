﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;

public class MapController
{
    public WaveController waveController;

    private Tower _towerPrefab;
    private string _mapName;
    private MapData _mapData;

    public MapController(string mapName, MapData mapData)
    {
        _mapName = mapName;
        _mapData = mapData;
        _towerPrefab = PrefabLoader.GetPrefab<Tower>(CONSTANTS.TOWERS_TAG);
        InitializeWaveController();
        SubscribeEvents();
    }

    private void SubscribeEvents()
    {
        ClickHandler.FreePlaceClicked += PlaceTower;
    }

    private void InitializeWaveController()
    {
        var pathsLayerData = Array.FindAll(_mapData.layers, (layerData) => { return layerData.name.ToLower().Contains(CONSTANTS.PATHS_TAG.ToLower()); });
        var paths = new Dictionary<string, ObjectData[]>();
        foreach (var path in pathsLayerData)
            paths.Add(path.name, path.objects);

        var wavesFileName = string.Format("{0} {1}", _mapName, CONSTANTS.WAVES_TAG);
        var wavesFile = Resources.Load<TextAsset>(wavesFileName);
        var wavesData = JsonUtility.FromJson<LevelWavesData>(wavesFile.text);
        waveController = new WaveController(wavesData, paths);
    }

    private void PlaceTower(FreePlacement place)
    {
        if (!place.IsFree)
        {
            Debug.Log("This place is occupied");
            return;
        }

        var tower = UnityEngine.Object.Instantiate(_towerPrefab);
        var position = place.transform.position;
        position.z = 0f;
        tower.transform.position = position;
        place.AddTower(tower);
        tower.Initialize(this, "MagicTower", 0);
    }
}