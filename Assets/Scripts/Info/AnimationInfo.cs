﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AnimationType { Idle, Walk, Attack }
public enum AnimationDirection { North, Side, South, Unknown };

public abstract class AnimationInfo
{
    private Dictionary<string, Sprite[]> _content;
    private Sprite[] _atlasSprites;
    protected readonly string _targetName;

    public AnimationInfo(string atlasName, string targetName)
    {
        _targetName = targetName;
        _atlasSprites = Resources.LoadAll<Sprite>(atlasName);
        _content = new Dictionary<string, Sprite[]>();
    }

    public Sprite[] this[string animName]
    {
        get
        {
            if (string.IsNullOrEmpty(animName))
                return null;

            Sprite[] sprites = null;
            _content.TryGetValue(animName, out sprites);

            return sprites;
        }
    }

    protected bool TryToAddAnimation(AnimationType type, AnimationDirection direction)
    {
        var animName = string.Format("{0}_{1}_{2}", _targetName, type.ToString(), direction.ToString());
        return TryToAddAnimation(animName);
    }

    protected bool TryToAddAnimation(string animName)
    {
        var sprites = TryToLoadAnimation(animName);
        if (sprites != null)
        {
            _content.Add(animName, sprites);
            return true;
        }

        return false;
    }

    private Sprite[] TryToLoadAnimation(string animationName)
    {
        var sprites = new List<Sprite>();
        foreach (var sprite in _atlasSprites)
        {
            if (sprite.name.StartsWith(animationName))
                sprites.Add(sprite);
        }

        if (sprites.Count == 0)
            return null;

        sprites.Sort(SpriteComparator);
        return sprites.ToArray();
    }

    private int SpriteComparator(Sprite sprite1, Sprite sprite2)
    {
        var sprite1Num = GetFrameNumber(sprite1.name);
        var sprite2Num = GetFrameNumber(sprite2.name);

        return sprite1Num.CompareTo(sprite2Num);
    }

    private int GetFrameNumber(string frameName)
    {
        if (string.IsNullOrEmpty(frameName))
            return -1;
        if (frameName.Length < 2)
            return -1;

        var lastIndex = frameName.LastIndexOf("_");
        if (lastIndex < 0)
            return -1;

        var textNum = frameName.Substring(lastIndex + 1);
        int num;
        if (!int.TryParse(textNum, out num))
            return -1;

        return num;
    }
}