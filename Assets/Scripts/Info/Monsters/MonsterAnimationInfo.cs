﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAnimationInfo : AnimationInfo
{
    public MonsterAnimationInfo(string atlasName, string targetName): base(atlasName, targetName)
    {
        TryToLoadWalkAnimation();
    }

    private void TryToLoadWalkAnimation()
    {
        TryToAddAnimation(AnimationType.Walk, AnimationDirection.North);
        TryToAddAnimation(AnimationType.Walk, AnimationDirection.Side);
        TryToAddAnimation(AnimationType.Walk, AnimationDirection.South);
    }
}
