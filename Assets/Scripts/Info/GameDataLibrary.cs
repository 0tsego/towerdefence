﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDataLibrary
{
    private const string MONSTERS_DESC_FILE_PATH = "MonstersDescription";
    private const string TOWERS_DESC_FILE_PATH = "TowersDescriptions";
    private const string PROJECTILES_DESC_FILE_PATH = "ProjectilesDescriptions";

    private static GameDataLibrary _instance;
    private MonstersDescription _monstersDescription;
    private Dictionary<string, TowerData> _towersDescription;
    private Dictionary<string, ProjectileData> _projectilesDescription;

    public static GameDataLibrary Instance
    {
        get
        {
            if(_instance == null)
                _instance = new GameDataLibrary();

            return _instance;
        }
    }

    private GameDataLibrary()
    {
        LoadMonstersData();
        LoadTowersData();
        LoadProjectilesData();
    }

    public MonsterData GetMonsterData(string name)
    {
        if (_monstersDescription == null)
            return null;

        return _monstersDescription[name];
    }

    public TowerData GetTowerData(string name)
    {
        if (_towersDescription == null)
            return null;

        TowerData towerData = null;
        _towersDescription.TryGetValue(name, out towerData);

        return towerData;
    }

    public ProjectileData GetProjectilesData(string name)
    {
        if (_projectilesDescription == null)
            return null;

        ProjectileData projectileData = null;
        _projectilesDescription.TryGetValue(name, out projectileData);

        return projectileData;
    }

    private void LoadMonstersData()
    {
        var file = Resources.Load<TextAsset>(MONSTERS_DESC_FILE_PATH);
        if (file != null && !string.IsNullOrEmpty(file.text))
            _monstersDescription = JsonUtility.FromJson<MonstersDescription>(file.text);
    }

    private void LoadTowersData()
    {
        _towersDescription = new Dictionary<string, TowerData>();

        var files = Resources.LoadAll<TextAsset>(TOWERS_DESC_FILE_PATH);
        if (files == null)
            return;

        foreach(var file in files)
        {
            if (file != null && !string.IsNullOrEmpty(file.text))
            {
                var entityData = JsonUtility.FromJson<TowerData>(file.text);
                entityData.name = file.name;
                _towersDescription.Add(file.name, entityData);
            }
        }
    }

    private void LoadProjectilesData()
    {
        _projectilesDescription = new Dictionary<string, ProjectileData>();

        var files = Resources.LoadAll<TextAsset>(PROJECTILES_DESC_FILE_PATH);
        if (files == null)
            return;

        foreach (var file in files)
        {
            if (file != null && !string.IsNullOrEmpty(file.text))
            {
                var entityData = JsonUtility.FromJson<ProjectileData>(file.text);
                entityData.name = file.name;
                _projectilesDescription.Add(file.name, entityData);
            }
        }
    }
}