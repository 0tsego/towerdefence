﻿public static class CONSTANTS
{
    public const string FREEPLACEMENTS_TAG = "FreePlacements";
    public const string FREEPLACEMENT_TAG = "FreePlacement";
    public const string PATHS_TAG = "Path";
    public const string TOWERS_TAG = "Tower";
    public const string MONSTERS_TAG = "Monster";
    public const string WAVES_TAG = "waves";
    public const string LEVEL_BACKGROUND_IMAGE_TAG = "Background";
}