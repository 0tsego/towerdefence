﻿using UnityEngine;
using System;

[Serializable]
public class ProjectileData: EntityData
{
    public float speed;
    public string type;
    public float damage;

    private ProjectileAnimationInfo _animationInfo;

    protected override AnimationInfo animationInfo
    {
        get
        {
            if (_animationInfo == null)
                _animationInfo = new ProjectileAnimationInfo(atlas, name);

            return _animationInfo;
        }
    }

    public Sprite[] GetMainAnimation()
    {
        var animName = string.Format("{0}_Main", name);
        return animationInfo[animName];
    }
}