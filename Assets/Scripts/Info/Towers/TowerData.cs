﻿using UnityEngine;
using System;

[Serializable]
public class TowerData: EntityData
{
    [SerializeField]
    private TowerLevelData[] levels;

    [NonSerialized]
    public int currentLevel = 0;

    private TowerAnimationInfo[] _animationInfos;

    protected override AnimationInfo animationInfo
    {
        get
        {
            if (_animationInfos == null)
                _animationInfos = new TowerAnimationInfo[levels.Length];

            if (_animationInfos[currentLevel] == null)
                _animationInfos[currentLevel] = new TowerAnimationInfo(atlas, name, currentLevel);

            return _animationInfos[currentLevel];
        }
    }

    public Sprite[] GetIdleAnim()
    {
        var animName = string.Format("{0}_{1}_{2}", name, currentLevel, AnimationType.Idle.ToString());
        return GetAnimation(animName);
    }

    public Sprite[] GetAttackAnim()
    {
        var animName = string.Format("{0}_{1}_{2}", name, currentLevel, AnimationType.Attack.ToString());
        return GetAnimation(animName);
    }

    public Vector2 AttackPoint
    {
        get { return levels[currentLevel].attackPoint; }
    }

    public float Range
    {
        get { return levels[currentLevel].range; }
    }

    public float SqrRange
    {
        get { return Range * Range; }
    }

    public string ProjectileName
    {
        get { return levels[currentLevel].projectile; }
    }
}