﻿public class TowerAnimationInfo: AnimationInfo
{
    public TowerAnimationInfo(string atlasName, string targetName, int level) : base(atlasName, targetName)
    {
        TryToAddAnimation(AnimationType.Idle, level);
        TryToAddAnimation(AnimationType.Attack, level);
    }

    protected bool TryToAddAnimation(AnimationType type, int level)
    {
        var animName = string.Format("{0}_{1}_{2}", _targetName, level.ToString(), type.ToString());
        return TryToAddAnimation(animName);
    }
}