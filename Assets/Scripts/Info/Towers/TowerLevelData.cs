﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class TowerLevelData
{
    public string projectile;
    public float attackSpeed; //count in second
    public float rechargeSpeed; //count in second
    public Vector2 attackPoint;
    public float range = 200; //in pixels
}