﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public abstract class EntityData
{
    public string name;
    public string atlas;

    protected abstract AnimationInfo animationInfo { get; }

    public Sprite[] GetAnimation(string animationName)
    {
        if (animationInfo == null)
            return null;

        return animationInfo[animationName];
    }
}