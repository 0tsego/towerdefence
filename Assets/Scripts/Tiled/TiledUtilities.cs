﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TiledUtilities
{
    public static TilesetData FindTilesetData(MapData mapData, int gid)
    {
        TilesetData closerTilesetData = null;
        foreach (var tilesetData in mapData.tilesets)
        {
            if (gid >= tilesetData.firstgid)
                closerTilesetData = tilesetData;
        }

        return closerTilesetData;
    }
}