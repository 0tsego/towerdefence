﻿using UnityEngine;
using System;

[Serializable]
public class TilesetData
{
    public int columns;
    public int firstgid;
    public string image;
    public int imageheight;
    public int imagewidth;
    public int margin;
    public string name;
    public int spacing;
    public int tilecount;
    public int tileheight;
    public int tilewidth;

    private Texture2D _texture;
    public Texture2D Texture
    {
        get
        {
            if (_texture == null)
            {
                _texture = Resources.Load<Texture2D>(name);
            }

            return _texture;
        }
    }

}