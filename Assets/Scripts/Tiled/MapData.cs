﻿using System;

[Serializable]
public class MapData
{
    public int height;
    public int width;
    public int tileheight;
    public int tilewidth;
    public LayerData[] layers;
    public TilesetData[] tilesets;

    public int HeightInPixels
    {
        get
        {
            return height * tileheight;
        }
    }

    public int WidthInPixels
    {
        get
        {
            return width * tilewidth;
        }
    }
}