﻿using UnityEngine;
using System;

[Serializable]
public class LayerData
{
    [SerializeField]
    private int[] data;
    public string name;
    public string image;
    public bool visible;
    public string type;
    public int width;
    public int height;
    public float x;
    public float y;
    public ObjectData[] objects;

    private int[,] _structure;
    public int[,] Structure
    {
        get
        {
            if(_structure == null && data != null && data.Length > 0)
            {
                _structure = new int[height, width];
                var arrayIndexer = 0;

                for( var i = 0; i < height; i++ )
                {
                    for( var j = 0; j < width; j ++ )
                    {
                        _structure[i, j] = data[arrayIndexer++];
                    }
                }
            }

            return _structure;
        }
    }

    public string ImageName
    {
        get
        {
            if (string.IsNullOrEmpty(image))
                return string.Empty;

            var imageName = string.Empty;
            var lastIndex = image.LastIndexOf('/');
            if (lastIndex >= 0 && image.Length > 1)
                imageName = image.Substring(lastIndex + 1);
            else
                imageName = image;

            if (!string.IsNullOrEmpty(imageName))
                imageName = imageName.Replace(".png", "");

            return imageName;
        }
    }
}