﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;

public class Main : MonoBehaviour
{
    public static Main Instance { get; private set; }
    public LeanSideCamera2D leanSideCamera;

    private void Awake()
    {
        if(Instance != null)
        {
            Debug.LogWarning("You're trying to create an additional instance of Main! This is not allowed!");
            Destroy(gameObject);
            return;
        }

        Instance = this;

        var mapVisualizer = new MapVisualizer("stage 2");
        var mapCenter = mapVisualizer.CalculateMapCenterPosition();
        Camera.main.transform.position = new Vector3(mapCenter.x, mapCenter.y, Camera.main.transform.position.z);
        var orthoSize = mapVisualizer.CalculateRecommendOrthoSize();
        leanSideCamera.Maximum = orthoSize;
        leanSideCamera.Minimum = orthoSize * 0.7f;
        Camera.main.orthographicSize = orthoSize;
    }
}
