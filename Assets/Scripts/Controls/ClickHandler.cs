﻿using UnityEngine;
using System;

public class ClickHandler : MonoBehaviour 
{
    public static event Action<FreePlacement> FreePlaceClicked;

	private void Update () 
	{
	    if(Input.GetMouseButtonDown(0))
        {
            ProcessClick();
        }
	}

    public void ProcessClick()
    {
        //Debug.Log("Mouse screen position is " + Input.mousePosition);
        var clickWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        clickWorldPos.z = 0f;
        //Debug.Log("Mouse world position is " + clickWorldPos);
        var freePlace = GetObject(clickWorldPos, "Free placements");
        if( freePlace != null && FreePlaceClicked != null )
            FreePlaceClicked(freePlace.GetComponent<FreePlacement>());
    }

    public GameObject GetObject(Vector2 origin, string layerName)
    {
        var layerIndex = LayerMask.NameToLayer(layerName);
        int mask = 1 << layerIndex;
        var hit = Physics2D.Raycast(origin, Vector2.up, 1f, mask);

        if(hit.collider == null)
        {
            //Debug.Log("Nothing here");
            return null;
        }

        Debug.Log(hit.collider.gameObject.name + " found");
        return hit.collider.gameObject;
    }

    private void UnsubscribeAllEvents()
    {
        FreePlaceClicked = null;
    }

    private void OnDestroy()
    {
        UnsubscribeAllEvents();
    }
}